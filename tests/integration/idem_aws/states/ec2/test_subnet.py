import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
# For some reason, localstack pro always times out at creating a vpc fixture. Hence, skip this test in localstack pro for now.
async def test_subnet(hub, ctx, aws_ec2_vpc):
    # Create subnet
    ipv4_cidr_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 29
    )
    subnet_temp_name = "idem-test-subnet-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": subnet_temp_name}]
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert ipv4_cidr_block == resource.get("CidrBlock")
    resource_id = resource.get("SubnetId")

    # Test associating ipv6 cidr block and adding tags
    ipv6_cidr_block = "2a05:d01c:74f:7200::/64"
    tags.append(
        {
            "Key": f"idem-test-subnet-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-subnet-value-{str(uuid.uuid4())}",
        }
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=resource_id,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=tags,
        ipv6_cidr_block=ipv6_cidr_block,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("Ipv6CidrBlockAssociationSet")[0].get("Ipv6CidrBlockState").get(
        "State"
    ) in ["associating", "associated"]
    assert ipv6_cidr_block == resource.get("Ipv6CidrBlockAssociationSet")[0].get(
        "Ipv6CidrBlock"
    )
    assert tags == resource.get("Tags")

    # Test deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=resource_id,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    # Make sure "Ipv6CidrBlockAssociationSet" remains the same
    assert ret.get("old_state").get("Ipv6CidrBlockAssociationSet") == resource.get(
        "Ipv6CidrBlockAssociationSet"
    )
    assert tags == resource.get("Tags")

    # Describe subnet
    describe_ret = await hub.states.aws.ec2.subnet.describe(ctx)
    assert resource_id in describe_ret

    # Delete subnet
    ret = await hub.states.aws.ec2.subnet.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
