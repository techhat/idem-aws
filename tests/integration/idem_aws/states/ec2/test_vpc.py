import copy
import random
import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_vpc(hub, ctx):
    # Create vpc
    vpc_temp_name = "idem-test-vpc-" + str(uuid.uuid4())
    num: () = lambda: random.randint(0, 255)
    cidr_block_association_set = [{"CidrBlock": f"192.168.{num()}.0/24"}]
    tags = [{"Key": "Name", "Value": vpc_temp_name}]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cidr_block_association_set[0].get("CidrBlock") == resource.get(
        "CidrBlockAssociationSet"
    )[0].get("CidrBlock")
    assert tags == resource.get("Tags")
    resource_id = resource.get("VpcId")

    # Test associating cidr block and adding tags
    cidr_block_association_set.append({"CidrBlock": f"192.168.{num()}.0/24"})
    tags.append(
        {
            "Key": f"idem-test-vpc-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-vpc-value-{str(uuid.uuid4())}",
        }
    )
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=resource_id,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cidr_block_association_set[0].get("CidrBlock") == resource.get(
        "CidrBlockAssociationSet"
    )[0].get("CidrBlock")
    assert cidr_block_association_set[1].get("CidrBlock") == resource.get(
        "CidrBlockAssociationSet"
    )[1].get("CidrBlock")
    assert tags == resource.get("Tags")

    # Test disassociating cidr and deleting tags
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=resource_id,
        cidr_block_association_set=copy.deepcopy([cidr_block_association_set[0]]),
        tags=[tags[0]],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cidr_block_association_set[0].get("CidrBlock") == resource.get(
        "CidrBlockAssociationSet"
    )[0].get("CidrBlock")
    assert resource.get("CidrBlockAssociationSet")[0].get("CidrBlockState").get(
        "State"
    ) in ["associating", "associated"]
    assert cidr_block_association_set[1].get("CidrBlock") == resource.get(
        "CidrBlockAssociationSet"
    )[1].get("CidrBlock")
    assert resource.get("CidrBlockAssociationSet")[1].get("CidrBlockState").get(
        "State"
    ) in ["disassociating", "disassociated"]
    assert [tags[0]] == resource.get("Tags")

    # Describe vpc
    describe_ret = await hub.states.aws.ec2.vpc.describe(ctx)
    assert resource_id in describe_ret

    # Delete vpc
    ret = await hub.states.aws.ec2.vpc.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
