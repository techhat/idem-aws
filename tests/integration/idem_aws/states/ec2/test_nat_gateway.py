import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_nat_gateway(hub, ctx, aws_ec2_subnet):
    # Create nat_gateway
    nat_gateway_id = "idem-test-nat-gateway-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": nat_gateway_id},
    ]

    ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=nat_gateway_id,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        client_token=None,
        connectivity_type="private",
        tags=tags,
    )
    created_nat_gateway_id = ret["new_state"]["NatGatewayId"]

    # verify that created nat_gateway is present
    describe_ret = await hub.states.aws.ec2.nat_gateway.describe(ctx)
    assert created_nat_gateway_id in describe_ret
    assert ret["new_state"]["Tags"] == tags
    assert ret["new_state"]["ConnectivityType"] == "private"

    # update tags
    new_tags = [
        {"Key": "new-name", "Value": created_nat_gateway_id},
    ]
    ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=created_nat_gateway_id,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        client_token=None,
        connectivity_type="private",
        tags=new_tags,
    )
    assert ret["new_state"]["Tags"] == new_tags

    # Delete instance
    ret = await hub.states.aws.ec2.nat_gateway.absent(ctx, name=created_nat_gateway_id)
    assert f"Deleted '{created_nat_gateway_id}'" in ret["comment"]
    assert ret["new_state"]["State"] == "deleted"

    # Deleting the same instance again should throw error
    ret = await hub.states.aws.ec2.nat_gateway.absent(ctx, name=created_nat_gateway_id)
    assert f"'{created_nat_gateway_id}' is already in deleted state." in ret["comment"]
