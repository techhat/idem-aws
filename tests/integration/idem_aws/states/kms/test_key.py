import uuid

import pytest


@pytest.mark.asyncio
async def test_key(hub, ctx):
    # Create key
    name = "idem-test-key-" + str(uuid.uuid4())
    tags = [{"TagKey": "Name", "TagValue": name}]
    ret = await hub.states.aws.kms.key.present(
        ctx,
        name=name,
        description=f"Description for {name}",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state")
    assert ret.get("new_state")
    resource = ret.get("new_state")
    assert f"Description for {name}" == resource.get("Description")
    resource_id = resource.get("KeyId", None)
    assert resource_id

    # Verify creation with tags
    describe_ret = await hub.states.aws.kms.key.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.kms.key.present"], "tags", tags
    )

    # Update tags
    tags = [
        {"TagKey": "Name", "TagValue": name},
        {"TagKey": "test-1", "TagValue": "test-1-val"},
        {"TagKey": "test-2", "TagValue": "test-2-val"},
    ]
    ret = await hub.states.aws.kms.key.present(
        ctx,
        name=resource_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "update" in ret["comment"].lower()
    assert "tags" in ret["comment"].lower()

    # Verify update with tags
    describe_ret = await hub.states.aws.kms.key.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.kms.key.present"], "tags", tags
    )

    # Disable key
    ret = await hub.states.aws.kms.key.present(
        ctx, name=resource_id, key_state="Disabled"
    )
    assert ret["result"], ret["comment"]

    # Verify key_state == Disabled
    describe_ret = await hub.states.aws.kms.key.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.kms.key.present"], "key_state", "Disabled"
    )

    # Enable key
    ret = await hub.states.aws.kms.key.present(
        ctx, name=resource_id, key_state="Enabled"
    )
    assert ret["result"], ret["comment"]

    # Verify key_state == Enabled
    describe_ret = await hub.states.aws.kms.key.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.kms.key.present"], "key_state", "Enabled"
    )

    # Delete Key - schedule for deletion
    ret = await hub.states.aws.kms.key.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("new_state").get("KeyState", None) == "PendingDeletion"
