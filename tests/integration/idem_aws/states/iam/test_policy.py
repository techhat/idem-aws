import json
import uuid

import pytest


@pytest.mark.asyncio
async def test_policy(hub, ctx):
    policy_temp_name = "idem-test-policy-" + str(uuid.uuid4())
    policy_document = {
        "Statement": [
            {"Action": ["ec2:CreateVpc"], "Effect": "Allow", "Resource": "*"}
        ],
        "Version": "2012-10-17",
    }

    description = "Idem IAM policy test description"
    tags = [{"Key": "Name", "Value": policy_temp_name}]
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=policy_temp_name,
        policy_name=policy_temp_name,
        policy_document=policy_document,
        description=description,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert policy_temp_name == resource.get("PolicyName")
    assert description == resource.get("Description")
    assert tags == resource.get("Tags")
    resource_id = resource.get("Arn")
    get_ret = await hub.exec.boto3.client.iam.get_policy_version(
        ctx=ctx, PolicyArn=resource_id, VersionId=resource.get("DefaultVersionId")
    )
    assert get_ret["result"]
    assert policy_document == get_ret["ret"].get("PolicyVersion").get("Document")

    # Test updating policy document and adding tags
    tags.append(
        {
            "Key": f"idem-test-iam-policy-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-iam-policy-value-{str(uuid.uuid4())}",
        }
    )
    policy_document = '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=resource_id,
        policy_name=policy_temp_name,
        policy_document=policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("Tags")

    get_ret = await hub.exec.boto3.client.iam.get_policy_version(
        ctx=ctx, PolicyArn=resource_id, VersionId=resource.get("DefaultVersionId")
    )
    assert get_ret["result"]
    assert policy_document == json.dumps(
        get_ret["ret"].get("PolicyVersion").get("Document")
    )

    # Test deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=resource_id,
        policy_name=policy_temp_name,
        policy_document=policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("Tags")

    # Describe IAM policy
    describe_ret = await hub.states.aws.iam.policy.describe(ctx)
    assert resource_id in describe_ret
    resource = describe_ret[resource_id].get("aws.iam.policy.present")
    for describe_param in resource:
        if "tags" in describe_param:
            assert tags == describe_param.get("tags")

    # Delete IAM policy
    ret = await hub.states.aws.iam.policy.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
