import uuid

import pytest


@pytest.mark.asyncio
async def test_role_policy(hub, ctx, aws_iam_role):
    # Create IAM role policy
    role_policy_temp_name = "idem-test-role-policy-" + str(uuid.uuid4())
    role_name = aws_iam_role.get("RoleName")
    policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Action": "s3:ListBucket","Resource": "arn:aws:s3:::example_bucket"}}'
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    policy_name = resource.get("PolicyName")

    # Describe IAM role policy
    describe_ret = await hub.states.aws.iam.role_policy.describe(ctx)
    assert policy_name in describe_ret

    # Delete IAM role policy
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx, name=policy_name, role_name=role_name
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
