import uuid

import pytest


@pytest.mark.asyncio
async def test_instance_profile(hub, ctx, aws_iam_role):
    # Updating tags on existing instance profile is not being tested,
    # since it is failing against localstack but passing with a real AWS

    # Create IAM instance_profile
    instance_profile_temp_name = "idem-test-instance_profile-" + str(uuid.uuid4())
    path = "/test/"
    tags = [{"Key": "Name", "Value": instance_profile_temp_name}]
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        path=path,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert path == resource.get("Path")
    assert instance_profile_temp_name == resource.get("InstanceProfileName")
    assert tags == resource.get("Tags")

    # Set the role on the instance_profile
    role_1 = aws_iam_role.get("RoleName")
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        roles=[{"RoleName": role_1}],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_1 == resource.get("Roles")[0].get("RoleName")

    # Create another role
    role_2 = "idem-test-role-" + str(uuid.uuid4())
    assume_role_policy_document = (
        "{'Statement': [{'Action': 'sts:AssumeRole', 'Effect': 'Allow',"
        " 'Principal': {'Service': 'spot.amazonaws.com'}}], 'Version': ''2012-10-17'' }"
    )
    ret = await hub.states.aws.iam.role.present(
        ctx, name=role_2, assume_role_policy_document=assume_role_policy_document
    )
    assert ret["result"], ret["comment"]
    assert role_2 == ret["new_state"]["RoleName"]

    # Update instance profile with the second role
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        roles=[{"RoleName": role_2}],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    assert role_2 == resource["Roles"][0]["RoleName"]

    # Describe IAM instance_profile
    describe_ret = await hub.states.aws.iam.instance_profile.describe(ctx)
    assert instance_profile_temp_name in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[instance_profile_temp_name]["aws.iam.instance_profile.present"],
        "tags",
        tags,
    )

    # Remove role from instance profile
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        roles=[],
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    assert not resource.get("Roles")

    # Delete IAM instance_profile
    ret = await hub.states.aws.iam.instance_profile.absent(
        ctx,
        name=instance_profile_temp_name,
    )
    assert ret["result"], ret["comment"]

    # Delete roles created by the test
    ret = await hub.states.aws.iam.role.absent(ctx, name=role_2)
    assert ret["result"], ret["comment"]
