import uuid

import pytest

AWS_Org_Not_In_Use_Exception = "AWSOrganizationsNotInUseException"


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization(hub, ctx):
    name = "idem-test-create-org-" + str(uuid.uuid4())

    # create organization
    present_ret = await hub.states.aws.organizations.organization.present(
        ctx, name=name, feature_set="ALL"
    )

    existing_or_created_org_id = present_ret.get("new_state")["Organization"]["Id"]

    if (
        hub.states.aws.organizations.organization.ALREADY_EXISTS
        in present_ret["comment"]
    ):
        assert present_ret["result"], present_ret["comment"]
        assert present_ret.get("old_state")
        assert present_ret.get("new_state")
    else:
        assert present_ret["result"], present_ret["comment"]
        assert not present_ret.get("old_state")

        assert present_ret.get("new_state")
        new_state = present_ret.get("new_state")
        assert new_state

        hub.tool.org_test_util.validate_organization(new_state)

    # describe organization
    describe_ret = await hub.states.aws.organizations.organization.describe(ctx)

    # check if described org_id matches with the created org_id
    assert existing_or_created_org_id in describe_ret

    describe_org_response = describe_ret[existing_or_created_org_id].get(
        "aws.organizations.organization.present"
    )

    test_response = {
        "Organization": {
            "Id": describe_org_response[0].get("id"),
            "Arn": describe_org_response[1].get("arn"),
            "MasterAccountArn": describe_org_response[2].get("master_account_arn"),
            "MasterAccountId": describe_org_response[3].get("master_account_id"),
            "MasterAccountEmail": describe_org_response[4].get("master_account_email"),
            "FeatureSet": describe_org_response[5].get("feature_set"),
            "AvailablePolicyTypes": describe_org_response[6].get(
                "available_policy_types"
            ),
        }
    }
    hub.tool.org_test_util.validate_organization(test_response)

    # try to delete organization if present
    delete_ret = await hub.states.aws.organizations.organization.absent(ctx, name=name)

    if delete_ret["result"]:
        describe_after_delete = (
            await hub.states.aws.organizations.organization.describe(ctx)
        )
        assert not bool(describe_after_delete)
