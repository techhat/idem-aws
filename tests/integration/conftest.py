import asyncio
import copy
import pathlib
import random
import string
import sys
import tempfile
import unittest.mock as mock
import uuid
import zipfile
from typing import Any
from typing import ByteString
from typing import Dict
from typing import List
from typing import Tuple

import dict_tools
import pop.hub
import pytest
from Cryptodome.PublicKey import RSA


# ================================================================================
# pop fixtures
# ================================================================================
@pytest.fixture(scope="module", autouse=True)
def acct_subs() -> List[str]:
    return ["aws"]


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_aws"


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="hub")
def tpath_hub(code_dir, event_loop):
    """
    Add "idem_plugin" to the test path
    """
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        hub.pop.sub.add(dyne_name="idem")
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)

        yield hub


@pytest.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)

    # Add the profile to the account
    if hub.OPT.acct.acct_file and hub.OPT.acct.acct_key:
        await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
        ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)
    else:
        hub.log.warning("No credentials found, assuming localstack on localhost")
        ctx.acct = dict(
            region_name="us-west-2",
            endpoint_url="http://0.0.0.0:4566",
            aws_access_key_id="localstack",
            aws_secret_access_key="localstack",
        )

    yield ctx


# --------------------------------------------------------------------------------


# ================================================================================
# AWS resource fixtures
# ================================================================================
@pytest.fixture(scope="module")
async def aws_ec2_subnet(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-test-subnet-" + str(uuid.uuid4())
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 29
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=[{"Key": "Name", "Value": subnet_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.subnet.absent(ctx, name=after["SubnetId"])
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_vpc(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_temp_name = "idem-test-vpc-" + str(uuid.uuid4())
    num: () = lambda: random.randint(0, 255)
    cidr_block_association_set = [{"CidrBlock": f"192.168.{num()}.0/24"}]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=[{"Key": "Name", "Value": vpc_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.vpc.absent(ctx, name=after["VpcId"])
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_transit_gateway(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 transit gateway for a module that needs it
    :return: a description of an ec2 transit gateway
    """
    transit_gateway_temp_name = "idem-test-transit-gateway-" + str(uuid.uuid4())
    transit_gateway_tags = [{"Key": "Name", "Value": transit_gateway_temp_name}]
    description = "my new transit gateway"
    ret = await hub.states.aws.ec2.transit_gateway.present(
        ctx,
        name=transit_gateway_temp_name,
        description=description,
        tags=transit_gateway_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.transit_gateway.absent(
        ctx, name=after["TransitGatewayId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_iam_role(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role for a module that needs it
    :return: a description of an IAM role
    """
    role_name = "idem-test-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Principal": {"Service": "ec2.amazonaws.com"},"Action": "sts:AssumeRole"}}'
    description = "Idem IAM role integration test fixture"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.iam.role.absent(ctx, name=after["RoleName"])
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="function")
async def aws_lambda_function(hub, ctx, zip_file, aws_iam_role) -> Dict[str, Any]:
    function_name = "test_idem_aws_function_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )

    ret = await hub.states.aws.λ.function.present(
        ctx,
        name=function_name,
        runtime="python3.7",
        handler="code.main",
        role=aws_iam_role["name"],
        code={"ZipFile": zip_file},
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]

    status, func = await hub.exec.aws.λ.function.get(ctx, name=function_name)
    assert status, func.exception

    yield func

    ret = await hub.states.aws.λ.function.absent(ctx, function_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]


@pytest.fixture(scope="function")
async def aws_s3_bucket(hub, ctx) -> Dict[str, Any]:
    bucket_name = "testIdemAWSBucket" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )

    result = await hub.states.aws.s3.bucket.present(
        ctx, name=bucket_name, object_lock_enabled_for_bucket=False
    )
    assert result["result"], result["comment"]
    assert result.get("name") == bucket_name

    status, bucket = await hub.exec.aws.s3.bucket.get(ctx, bucket_name)
    assert status, bucket.exception

    yield bucket

    ret = await hub.states.aws.s3.bucket.absent(ctx, name=bucket_name)
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="function")
async def aws_organization(hub, ctx) -> Dict[str, Any]:
    name = "idem-test-create-org-" + str(uuid.uuid4())

    # create organization
    result = await hub.states.aws.organizations.organization.present(
        ctx, name=name, feature_set="ALL"
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.organization.absent(ctx, name=name)

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


# --------------------------------------------------------------------------------

# ================================================================================
# resource helpers
# ================================================================================
@pytest.fixture(scope="module", name="instance_name")
def aws_instance_name() -> str:
    yield "test-idem-cloud-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )


@pytest.fixture(scope="module")
def ssh_key_pair() -> Tuple[ByteString, ByteString]:
    key_pair = RSA.generate(1024)
    yield key_pair.export_key(), key_pair.publickey().export_key()


@pytest.fixture(scope="module")
def zip_file() -> ByteString:
    """
    Create a zip file for tests that use lambda functions
    """
    with tempfile.TemporaryDirectory() as tempdir:
        path = pathlib.Path(tempdir)
        z_path = path.joinpath("code.zip")

        with zipfile.ZipFile(z_path, "w") as myzip:
            myzip.writestr("code.py", "def main():\n\treturn 0")

        with open(z_path, "rb") as fh:
            yield fh.read()


# --------------------------------------------------------------------------------
