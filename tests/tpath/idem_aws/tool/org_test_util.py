import re

ORGANIZATION_ARN_FORMAT = "arn:aws:organizations::{0}:organization/{1}"
MASTER_ACCOUNT_ARN_FORMAT = "arn:aws:organizations::{0}:account/{1}/{0}"
ACCOUNT_ARN_FORMAT = "arn:aws:organizations::{0}:account/{1}/{2}"

ORG_ID_SIZE = 10
ORG_ID_REGEX = r"o-[a-z0-9]{%s}" % ORG_ID_SIZE


def validate_organization(hub, response):
    org = response["Organization"]
    assert sorted(org.keys()) == (
        [
            "Arn",
            "AvailablePolicyTypes",
            "FeatureSet",
            "Id",
            "MasterAccountArn",
            "MasterAccountEmail",
            "MasterAccountId",
        ]
    )

    assert re.compile(ORG_ID_REGEX).match(org["Id"])
    assert org["MasterAccountArn"] == MASTER_ACCOUNT_ARN_FORMAT.format(
        org["MasterAccountId"], org["Id"]
    )
    assert org["Arn"] == ORGANIZATION_ARN_FORMAT.format(
        org["MasterAccountId"], org["Id"]
    )
    assert org["FeatureSet"] in ["ALL", "CONSOLIDATED_BILLING"]
    assert org["AvailablePolicyTypes"] == [
        {"Type": "SERVICE_CONTROL_POLICY", "Status": "ENABLED"}
    ]
